﻿using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using opdracht04.Models;
using MongoDB.Driver;
using MongoDB.Bson;

namespace opdracht04.Hubs
{
    public class TodoHub : Hub
    {
        private IMongoDatabase mongoDatabase;

        //Generic method to get the mongodb database details  
        public IMongoDatabase GetMongoDatabase()
        {
            var mongoClient = new MongoClient("mongodb://localhost:27017");
            return mongoClient.GetDatabase("Embedded_Opdracht04");
        }


        public async Task SendTodo(string id, string category, string title, string status, string user)
        {
            var culturInfo = new CultureInfo("de-DE");
            var newId = int.Parse(id) + 1;

            mongoDatabase = GetMongoDatabase();
            var newTodo = new Todo { Id = newId, Category = category, Title = title, Status = status, User = user, DateTimeNow = DateTime.Now.ToString(culturInfo) };
            mongoDatabase.GetCollection<Todo>("Todos").InsertOne(newTodo);

            Todo todo = mongoDatabase.GetCollection<Todo>("Todos").Find<Todo>(k => k.Id == newId).FirstOrDefault();
            var objId = todo.ObjId.ToString();
            
            await Clients.All.SendAsync("ReceiveTodo", objId, newId, category, title, status, user, DateTime.Now.ToString(culturInfo));
        }

        public async Task DeleteTodo(string objId, string id)
        {
            var selectedObjId = new ObjectId(objId);

            await Clients.All.SendAsync("DeleteTodo", objId, id);

            mongoDatabase = GetMongoDatabase();
            var result = mongoDatabase.GetCollection<Todo>("Todos").DeleteOne<Todo>(k => k.ObjId == selectedObjId);
        }
    }
}
