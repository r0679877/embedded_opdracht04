﻿"use strict";

var connection = new signalR.HubConnectionBuilder().withUrl("/todoHub").build();

connection.on("ReceiveTodo", function (objId, id, category, title, status, user, dateTime) {

    var table = document.getElementById('listTable').insertRow(-1);
    table.id = id;
    var td0 = table.insertCell(0);
    var td1 = table.insertCell(1);
    var td2 = table.insertCell(2);
    var td3 = table.insertCell(3);
    var td4 = table.insertCell(4);
    var td5 = table.insertCell(5);
    var td6 = table.insertCell(6);
    
    td0.textContent = id;
    td1.textContent = category;
    td2.textContent = title;
    td3.textContent = status;
    td4.textContent = user;
    td5.textContent = dateTime;
    td6.innerHTML = "<input type=\"button\" value=\"Delete\" onclick=\"deleteTodo('" + objId + "','" + id + "')\" />";

    console.log("objID: " + objId + "| id: " + id);
});

connection.on("DeleteTodo", function (objId, id) {
    console.log("Delete with objId: " + objId + "| id: " + id);

    document.getElementById(id).remove();
});

connection.start().catch(function (err) {
    return console.error(err.toString());
});

document.getElementById("sendButton").addEventListener("click", function (event) {
    var category = document.getElementById("category").value;
    var title = document.getElementById("title").value;
    var element = document.getElementById("status");
    var status = element.options[element.selectedIndex].text
    var user = document.getElementById("user").value;

    var id = document.getElementById('listTable').rows.length -2;

    clearValue("category");
    clearValue("title");

    connection.invoke("SendTodo", id, category, title, status, user).catch(function (err) {
        return console.error(err.toString());
    });
    event.preventDefault();
});

function deleteTodo(objId, id) {
    connection.invoke("DeleteTodo", objId, id).catch(function (err) {
        return Console.error(err.toString());
    });
    event.preventDefault();
}

function clearValue(id) {
    document.getElementById(id).value = "";
}

function changeValue() {
    var color = $("option:selected", this).attr("class");
    $("#status").attr("class", color);
};