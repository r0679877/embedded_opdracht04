﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;
using System.ComponentModel.DataAnnotations;
using MongoDB.Bson;

namespace opdracht04.Models
{
    public class Todo
    {
        [BsonId]
        public ObjectId ObjId { get; set; }

        [Display(Name = "Todo Id")]
        public int Id { get; set; }
        [Display(Name = "Todo Category")]
        public string Category { get; set; }
        [Display(Name = "Todo Title")]
        public string Title { get; set; }
        [Display(Name = "Todo Status")]
        public string Status { get; set; }
        [Display(Name = "Todo User")]
        public string User { get; set; }
        [Display(Name = "Todo DateTimeNow")]
        public string DateTimeNow { get; set; }

    }
}
