﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace opdracht04.Models
{
    public class MongoSetting
    {
        public string ConnectionString;
        public string Database;
    }
}
