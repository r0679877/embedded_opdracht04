﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using opdracht04.Models;
using MongoDB.Driver;

namespace opdracht04.Controllers
{
    public class HomeController : Controller
    {
        private IMongoDatabase mongoDatabase;

        //Generic method to get the mongodb database details  
        public IMongoDatabase GetMongoDatabase()
        {
            var mongoClient = new MongoClient("mongodb://localhost:27017");
            return mongoClient.GetDatabase("Embedded_Opdracht04");
        }

        [HttpGet]
        public IActionResult listTodos()
        {
            //Get the database connection  
            mongoDatabase = GetMongoDatabase();
            //fetch the details from TodosDB and pass into view  
            var result = mongoDatabase.GetCollection<Todo>("Todos").Find(FilterDefinition<Todo>.Empty).ToList();
            return View(result);
        }

        [HttpGet]
        public IActionResult Delete(int? ObjId)
        {
            //Get the database connection  
            mongoDatabase = GetMongoDatabase();
            var result = mongoDatabase.GetCollection<Todo>("Todos").DeleteOne<Todo>(k => k.ObjId.ToString() == ObjId.ToString());

            return RedirectToAction("listTodos");
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
